<?php
//              FOR SEO AND DYNAMIC CONTENT FILL IN FROM PerkinsTop.php 
    $pageTitle = 'Chad Perkins acting resume';
    $pageDescription = 'C. Perkins jobs and projects from movies films starring co-starring featured part';
    $pageRobots = "NOINDEX, NOFOLLOW";
    $pageCanonical = 'Http://www.ChadPerkins.Actor.com/perkinsRepresentation.php';

//                                  REQUIRE FOR TOP OF PAGE, UP TO <head>
    require("layout/perkinsTop.php");
//                                  REQUIRE FOR HEADER OF PAGE (NAME + FACEBOOK + MAIL)
    require("layout/perkinsHeader.php");
//                                  REQUIRE FOR MENU OF SITE
    require("layout/perkinsMenu.php");
?>
<style>
.container{
        display: block;
        position: relative;
        width: 80%;
        margin: auto;
        font-weight: 300;
}
td{
        border-radius: 2em;
}
.container h2,h4{
        text-align: center;
        font-weight: 600;
}
.y{
        text-align: center;
}
.z{
        text-align: right;
}
</style>
<!--                                CONTENT FOR THIS PAGE-->
<div id="resumeCont">
        <div class='container'>
                <h2>Chad Perkins Resume</h2>
                <h4>Chad.t.Perkins@gmail.com<br>
                        (760)458-7030<br>
                        Non-Union, SAG/AFTRA Eligible<br>
                </h4>
                <br>
                <table class='table'>
                        <tbody>
<!-- FILMS -->                         
                                <tr class='danger'>
                                        <td style='border-radius:2em;display:block;'><h3>Film</h3></td>
                                </tr>                        
                                <tr class='success'>
                                        <td >The Reason</td>
                                        <td class='y'>Supporting</td>
                                        <td class='z'>Short and Feature, Dir. Meiko Taylor</td>
                                </tr>                                
                                <tr class='active'>
                                        <td>Yum Yum</td>
                                        <td class='y'>Lead</td>
                                        <td class='z'>Short, Dir. Andre Rhoden</td>
                                </tr>
                                <tr class='info'>
                                        <td>Psyanky</td>
                                        <td class='y'>Lead</td>
                                        <td class='z'>Sohrt, Dir. Robert Burdsall</td>
                                </tr>
                                <tr class='warning'>
                                        <td>Disconnected</td>
                                        <td class='y'>Supporting</td>
                                        <td class='z'>Feature, Dir. Eivid Nina Pedersen</td>
                                </tr>
                                <tr class='danger'>
                                        <td>Death, Sammy Baker And A Loaded .45</td>
                                        <td class='y'>Lead</td>
                                        <td class='z'>Short, Dir. Wey Wang</td>                                       
                                </tr> 
<!-- TV-->
                                <tr class='danger'>
                                        <td style='border-radius:2em;display:block;'><h3>Television</h3></td>
                                </tr>                        
                                <tr class='success'>
                                        <td>Baseball,Dennis And The French</td>
                                        <td class='y'>Supporting</td>
                                        <td class='z'>Claire Anett Prod., Dir. Paul Croshaw</td>
                                </tr>                                        
                                <tr class='active'>
                                        <td>Tape</td>
                                        <td class='y'>Lead</td>
                                        <td class='z'>Murxee Ent., Dir. Jacob Lucking</td>
                                </tr>
<!-- THEATRE-->
                                <tr class='danger'>
                                        <td style='border-radius:2em;display:block;'><h3>Theatre</h3></td>
                                </tr>                        
                                <tr class='success'>
                                        <td>Golden Boy</td>
                                        <td class='y'>Joe, Boxer</td>
                                        <td class='z'>Black box Theater.Eugehe Buica, Artistic Director</td>
                                </tr>                                
                                <tr class='active'>
                                        <td>A Mother**** With A Hat</td>
                                        <td class='y'>Jackie, Drug Addict</td>
                                        <td class='z'>Black Box Theater. Eugene Buica, Artistic Director</td>
                                </tr>
                                <tr class='info'>
                                        <td>Hurlyburly</td>
                                        <td class='y'>Eddie, Casting Director</td>
                                        <td class='z'>Black Box Theater. Eugene Buica, Artistic Director</td>
                                </tr>
                                <tr class='warning'>
                                        <td>Glengarry Glen Ross</td>
                                        <td class='y'>Aaronow, Salesman</td>
                                        <td class='z'>Black Box Theater. Eugene Buica, Artistic Director</td>
                                </tr>
                                <tr class='danger'>
                                        <td>Lone Star</td>
                                        <td class='y'>Roy, War Vet Brother</td>
                                        <td class='z'>Miracosta Theater. Dir. Eric Bishop</td>                                       
                                </tr> 
                                <tr class='success'>
                                        <td>Smudge</td>
                                        <td class='y'>Pete, Pollster</td>
                                        <td class='z'>Miracosta Theater. Dir. Tracy Williams</td>
                                </tr> 
                        </tbody>
                </table>                                
<!-- TRAINING-->
                <table class='table'>
                        <tbody>
                                
                                <tr class='danger'>
                                        <td style='border-radius:2em;display:block;'><h3>Training</h3></td>
                                </tr> 
                                <tr class='success'>
                                        <td>Jame's Franco PHWstudio4</td>
                                        <td class='y'>Meisner Technique/Improvisation by Kathleen Randazzo, Sean Barnes, Michael Bradley</td>
                                </tr>
                                <tr class='info'>
                                        <td>The Acting Corps</td>
                                        <td class='y'>The Professional Program<br>
                                                Acting Corps Technique(Chekov/Meisner)/Scene Study/Actor's Instrument/<br>
                                                On-Camera Stucy/Improv/Cold Reading/business of Showbusiness<br>
                                                by Corey Sorenson, Sydney Walsh, Jen McPherson, Eugene Buica.
                                        </td>
                                </tr>
                                <tr class='danger'>
                                        <td>Micacosta College Theater</td>
                                        <td>Acting 2/Scene Study/On-Camera. Strasber/Checkov by Eric Bishop and Tracy Williams</td>
                                </tr>
                                <tr class='success'>
                                        <td>Joanne Baron D.W. Brown Studio</td>
                                        <td class='y'>Meisner Technique by Joanne Baron</td>
                                </tr>
                        </tbody>
                </table>

<!-- TRAINING-->
                <table class='table'>
                        <tbody>
                                
                                <tr class='danger'>
                                        <td style='border-radius:2em;display:block;width:30%;'><h3>Skills</h3></td>
                                </tr>                           
                                <tr class="info">
                                        <td class='y'>Boarding, snowboarding, firearm training, swimming, car/motorcycle,
                                                hockey, basic sports in general, hikin, running, CPR, bodybuilding,<br>
                                                cross-training, rock climbing.</td>
                                </tr>
                        </tbody>
                </table>                

        </div>
</div>

<?php
//                                  REQUIRE FOR FOOTER, BEGINS: <footer>, ENDS: </body></html>
    require("layout/perkinsBottom.php");
?>
