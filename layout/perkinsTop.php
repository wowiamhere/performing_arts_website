<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="<?php echo $pageDescription; ?>" />

<!--         FOR TRANSLATION OPTION BY GOODLE-->
        <meta name="google" content="translate" />

<!--        FOR SEO, ROBOTS (search engine crawlers) CONTENT VALUE NOT SET YET!!!!!!!!!!!!!!!-->
        <?php
            if(isset($pageRobots))
                echo '<meta name="robots" content="' . $pageRobots . '" />';
        ?>

        <title><?php echo $pageTitle; ?></title>

<!--        FOR SEO CANONICAL IF $_GET HAS ANYTHING-->
    <?php
        if($_GET != null){
    ?>
        <link rel="canonical" href="<?php echo $pageCanonical; ?>" />
    <?php
        }
    ?>
<!--		STYLE SHEET-->
        <link rel="stylesheet" href="layout/perkinsStyle.css">
<!--            BOOTSTRAP-->
                <link rel='stylesheet' href='/media/data/bootstrap.min.css'></link>       

<!--		JQUERY-->
		<script src="/data/jquery-1.11.1.js"></script>
    </head>
    <body>
        

