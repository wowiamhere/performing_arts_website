<?php
//              FOR SEO AND DYNAMIC CONTENT FILL IN FROM PerkinsTop.php 
    $pageTitle = 'Chad Perkins headshots';
    $pageDescription = 'chad p. photographs.  His pictures, pics and stills from movies and tv shows.';
    $pageRobots = NULL;
    $pageCanonical = 'Http://www.ChadPerkins.Actor.com/perkinsPhotos.php';

//                                  REQUIRE FOR TOP OF PAGE, UP TO <head>
    require("layout/perkinsTop.php");
//                                  REQUIRE FOR HEADER OF PAGE (NAME + FACEBOOK + MAIL)
    require("layout/perkinsHeader.php");
?>

<!--                                CONTENT FOR THIS PAGE-->


<?php
//                                  REQUIRE FOR MENU OF SITE
    require("layout/perkinsMenu.php");
?>
<div id="headshotContainer">
        <h1 class="h1PicsPage">Headshots</h1>
                
        <div id="picWrapper">
                <div id="picCont">
                        <div id="pics">
							<div class="picsA">
								    <a class="sticker example-1" href="/media/pictures/1-500x750-55kb.jpg" target="_self"></a>   
							</div>
							<div class="picsA">
									<a class="sticker example-2" href="/media/pictures/2-500x750-45kb.jpg" target="_self"></a>
							</div>
							<div class="picsA">
									<a class="sticker example-3" href="/media/pictures/3-500x750-59kb.jpg" target="_self"></a>
							</div>
							<div class="picsA">
									<a class="sticker example-4" href="/media/pictures/4-500x750-59kb.jpg" target="_self"></a>
							</div>
							<div class="picsA">
									<a class="sticker example-5" href="/media/pictures/5-500x750-61kb.jpg" target="_self"></a>
							</div>
							<div class="picsA">
									<a class="sticker example-6" href="/media/pictures/6-500x750-49kb.jpg" target="_self"></a>
							</div>
							<div class="lastA">
									<a class="sticker example-7" href="/media/pictures/6-500x750-49kb.jpg" target="_self"></a>
							</div>
                        </div>
              </div>

<!--id='picWrapper'-->
        </div>
<!--id='headshotContainer-->
</div>

<script src="layout/sticker.min.js">
</script>
<script type="text/javascript">
        window.onload = function(){
                Sticker.init('.sticker');
        }
</script>
<?php
//                                  REQUIRE FOR FOOTER, BEGINS: <footer>, ENDS: </body></html>
    require("layout/perkinsBottom.php");
?>
