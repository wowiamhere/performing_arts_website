<?php
//              FOR SEO AND DYNAMIC CONTENT FILL IN FROM PerkinsTop.php 
    $pageTitle = 'Chad Perkins representation information.';
    $pageDescription = 'Perkins Chad agent and manager contact information for booking and auditions: contact  agency';
    $pageRobots = NULL;
    $pageCanonical = 'Http://www.ChadPerkins.Actor.com/perkinsRepresentation.php';

//                                  REQUIRE FOR TOP OF PAGE, UP TO <head>
    require("layout/perkinsTop.php");
//                                  REQUIRE FOR HEADER OF PAGE (NAME + FACEBOOK + MAIL)
    require("layout/perkinsHeader.php");
?>

<!--                                CONTENT FOR THIS PAGE-->

<?php
//                                  REQUIRE FOR MENU OF SITE
    require("layout/perkinsMenu.php");
?>
			<div class="repInfo">
				<div class="divRep">
					<div class="cMngr">
						<h1 class="x">YPS Management</h1>
						<h3><span>Amy Lord</span><br>Manager</h3>
						<p>14431 Ventura Blvd.<br/>
								Shermak Oaks, Ca 91423<br/>
								<a href="mailto:amyyps@gmail.com">AmyYps@gmail.com</a><br/>(310)980-6260
						</p>
						</br></br></br></br></br></br>
					</div>
					<div class="cAgent">
						<h1 class="x">Mogan Entertainment</h1>
						<h3><span>Michael Mogan</span><br>Theatrical Agent</h3>
						<p>4121 Radfford Ave<br/>
								Studio City, Ca 91604<br/>
								<a href="mailto:michael@MoganEntertainment.com">michael@MorganEntertainment.com</a><br/>(818)505-8140
						</p>
						</br></br></br></br></br></br>
					</div>
				</div>
			</div>

<?php
//                                  REQUIRE FOR FOOTER, BEGINS: <footer>, ENDS: </body></html>
    require("layout/perkinsBottom.php");
?>