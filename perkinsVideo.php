<?php
//              FOR SEO AND DYNAMIC CONTENT FILL IN FROM PerkinsTop.php 
    $pageTitle = 'Chad Perkins Video';
    $pageDescription = 'c. Perkins movie trailers includin his reel and any other video that shows his dramatic and comedic skills.';
    $pageRobots = NULL;
    $pageCanonical = 'Http://www.ChadPerkins.Actor.com/perkinsReelVideo.php';

//                                  REQUIRE FOR TOP OF PAGE, UP TO <head>
    require("layout/perkinsTop.php");
//                                  REQUIRE FOR HEADER OF PAGE (NAME + FACEBOOK + MAIL)
    require("layout/perkinsHeader.php");
?>

<!--                                CONTENT FOR THIS PAGE-->

<?php
	//                                  REQUIRE FOR MENU OF SITE
    require("layout/perkinsMenu.php");
?>
        <div id="videoPg">
				<h1 class="h1ReelPage">Performance Reel</h1>
			<div id="videoPgWrapper">
				<div id="reel">
					<iframe id="reelIframe" width="600" height="375" src="//www.youtube.com/embed/QTyC-Dc9A8I" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>

<?php
//                                  REQUIRE FOR FOOTER, BEGINS: <footer>, ENDS: </body></html>
    require("layout/perkinsBottom.php");
?>