# Performing Arts Page (actor)

- 9 page Entertainment Industry Actor's Webpage.  
- Career Information (resume, contact, etc).  
- Custom Coded all pages/capabilities/technologies:   
	+ Html, Css, Php, JavaScript, Jquery Social Media Buttons, Links And Sharing   
- no persistency  

**Live Website**  
https://performingartswebsite.herokuapp.com/  

**Repo**  
https://bitbucket.org/wowiamhere/performing_arts_website

**Online Portfolio**  
http://ZenCodeMaster.com
