<?php
//              FOR SEO AND DYNAMIC CONTENT FILL IN FROM PerkinsTop.php 
    $pageTitle = 'Perkins professional performer biography.';
    $pageDescription = 'Chad Perkins acting life and story, his personal and professional plans and goals.';
    $pageRobots = NULL;
    $pageCanonical = 'Http://chadPerkins.Actor.com/perkinsAbout.php';

//                                  REQUIRE FOR TOP OF PAGE, UP TO <head>
    require("layout/perkinsTop.php");
//                                  REQUIRE FOR HEADER OF PAGE (NAME + FACEBOOK + MAIL)
    require("layout/perkinsHeader.php");
?>
<!--                                CONTENT FOR THIS PAGE-->

<?php
//                                  REQUIRE FOR MENU OF SITE
    require("layout/perkinsMenu.php");
?>
		<div id="aboutCont">
			<div id="aboutCont2" class="aboutCont2">

<!--		FOR WRITING ABOUT-->			
			<div id="aboutContent" class="aboutCont">
				<h1 class="h1About">Chad Perkins Bio</h1>
				<p class="pAbout">Born and raised in <span>Carsbad, Ca</span>.&#160;<a href="/index.php">Chad Perkins</a> spent the majority of his life
						skateboarding in the <a href="http://www.caslusf.com"><span>California Amateur Skateboard League</span>.</a>
						He attended Miracosta College where he explored Psychology, Social Work and Biology. &#10;&#13;
						Due to the encouraging work of Cameron Crowe, Chad developed a spirit of inquiry in
						the arts and used his elective credits to enroll in acting classes.	
						He realized his potential as he developed in his Dramatic studies and immediagely
						fell in love with the Performing Arts.</br>
						In 2012, the California native moved to Hawaii to work on a commercial cruise line. 
						In 2013, he took his earnings and moved to LA (Los Angeles).  Chad saw 
						the potential in Film and Television and took advantage of the opportunities while 
						continuing to intensively study.</br>
						In addition to acting, Chad loves music, traveling and spending his free time with 
						family in San Diego, CA.
				</p>
<!--#aboutContent-->
			</div>

<!--		FOR PICTURE-->
			<div id="aboutPic" class="abtPic">
				<div class="aboutPicCont">
<!--	FIX LINK ADDRESS FOR PUBLISHING-->
					<a href="/" target="_self">
						<img src="/media/pictures/2comp31kb.jpg" alt="chat perkins biography"/>
					</a>	
				</div>
<!--  #aboutPic-->
			</div>

<!--#aboutCont2-->
			</div>
<!--  #aboutCont1-->
		</div>

<?php
//                                  REQUIRE FOR FOOTER, BEGINS: <footer>, ENDS: </body></html>
    require("layout/perkinsBottom.php");
?>

