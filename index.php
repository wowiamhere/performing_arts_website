<?php    
//              FOR SEO AND DYNAMIC CONTENT FILL IN FROM PerkinsTop.php 
    $pageTitle = 'Chad Perkins actor website.';
    $pageDescription= 'Chad Perkins career online resume, pictures, projects, contact, representation and audition information for Entertainment industry: Hollywood';
    $pageRobots = "INDEX FOLLOW";
    $pageCanonical = "Http://ChadPerkins.Actor.com";

//                                  REQUIRE FOR TOP PORTION OF PAGE (UP TO OPENING <head)
    require("layout/perkinsTop.php");
//                                  REQUIRE FOR HEADER SECTION (NAME + FACEBOOK + EMAIL)
    require("layout/perkinsHeader.php");
?>
			<script>
				var x = 0;
				var photos = 6;
				var NUM_PH = photos-1;
				var id = null;

				function exeFlashPhotos(){
					id = setInterval(flashPhotos, 3000);
				}
				function stp(){
					clearInterval(id);
				}
				function flashPhotos(){
					for(var i = 0;i < photos;++i){
						if(i === x)
							window.document.getElementsByClassName("img")[x].style.display = "";
						else
							window.document.getElementsByClassName("img")[i].style.display = "none";
					}
					if(x === NUM_PH)
						x = 0;
					else
						++x;
				}				
				window.onload = setInterval(flashPhotos, 3000);//exeFlashPhotos();
		</script>
<!--                    THIS PAGE CONTENT-->


<?php
//                                  REQUIRE FOR SITE MENU
    require("layout/perkinsMenu.php");
?>
	<div id="frontPageContI" class="frontPageContC">
		<div id="frontPageContI2" class="frontPageContC2">
<!--script for front page images-->	
		<div id="images">
			<div id="imgCont">

				<div class="img img1" style="display: none;">
					<img src="/media/pictures/1-500x750-55kb.jpg" alt="Chad Perkins Headshot" style="max-height: 30em;">
				</div>
				<div id="" class="img img1" style="display: none;">
					<img src="/media/pictures/2-500x750-45kb.jpg" alt="Chad Perkins Headshot" style="max-height: 30em;">
				</div>
				<div id="" class="img img1" style="display: none;">
					<img src="/media/pictures/3-500x750-59kb.jpg" alt="Chad Perkins Headshot" style="max-height: 30em;">
				</div>
				<div id="" class="img img2" style="display: none;">
					<img src="/media/pictures/4-500x750-59kb.jpg" alt="Chad Perkins Headshot" style="max-width: 87%;">
				</div>
				<div id="" class="img img2" style="display: none;">
					<img src="/media/pictures/5-500x750-61kb.jpg" alt="Chad Perkins Headshot" style="max-width: 87%;">
				</div>
				<div id="" class="img img2" style="display: none;">
					<img src="/media/pictures/6-500x750-49kb.jpg" alt="Chad Perkins Headshot" style="max-width: 87%;">
				</div>
			<!--#imgCont DIV-->
			</div>
			<!--#images div-->
		</div>
		<div id="frontSideI" class="frontSideC">
			<h1 class="h1MainPgTitle">Chad Perkins</h1>
			<h3 class="h3MainPgTitle"> Actor Writer Performer</h3>
			<p class="pMainPgTitle">
<!--
                                Chad's Television appearances include a Lead role on Director Luckingsproject: Tape.
				His films include Yum Yum, The Reason & Psyanky,all full lenght feature films.</br>
				Chad's commitment to his craft is evident in his Theatre repertoire: Golden Boy, HurlyBurly
				& Glengarry Glen Ross, amonst others.
				Please refer to Chad's resume for full credits.
                                </br>
-->
                                Yum Yum, staring Chad Perkins, was officially selected in 
                                <a href="http://www.pasadenaFilmFestival.org" target="_blank" title="chad Perkins nomination" style="position: relative">
                                The Pasadena Film Festival 
				</a>
                                February 11-15. This will be the west coast premiere of the film and the first screening in 
				Los Angles in 2015! 
                        </p>
                        <div style="position: relative;display:block;;margin:auto;width: 25%;">
                                <a href="http://www.pasadenaFilmFestival.org" target="_blank" title="chad Perkins nomination">
                                        <img src="/layout/pasadenaIntlFilmFest-125x85-5kb.jpg" alt="festival image" style=";">
                                </a>
                        </div>
                        <p class="pMainPgTitle">
                                Chad just signed with Mogan Entertainment in Los Angeles for Theatrical Representation.
                                </br>
				Chad is constantly updating his pictures: still shots, photo shoots or headshots,
				please stay tuned for behind the scenes pics!
				</br></br>
				His latest reel, all updated performances and appearances, are now available on
				<a href="/perkinsVideo.php" target="_self" title="chad perkins resume page">Chad Perkins performance & resume page</a>
				and on <a href="http://www.imdb.com/name/nm4377760/" target="_blank" title="imdb link to chad perkins">IMDB.com</a>
				</br>

			</p>

		</div>
<!--frontPageContC/I2-->
			</div>
<!--FRONT PAGE CONTAINER DIV-->		
	</div>

<?php
//                                  EQUIRE FOR FOOTER, BEGINS: <footer>, ENDS: </body></html>
 require("layout/perkinsBottom.php");
 ?>



